package com.example.test

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import com.example.test.R
import com.example.test.webservices.*


class MenuActivity : AppCompatActivity() {

    private lateinit var productsView: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        productsView = findViewById(R.id.productsView)
        getMenu()
    }

    private fun getMenu() {
        val okhttpclient =  OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://otf2.kubilabs.com/")
            .client(okhttpclient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(Api::class.java)

        val preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE)
        val apiKey = preferences.getString("apiKey", "")
        val deviceId = preferences.getString("deviceId", "")

        val call = service.getMenu(apiKey, deviceId)
        call.enqueue(object : Callback<MenuResponse> {
            override fun onResponse(call: Call<MenuResponse>, response: Response<MenuResponse>) {
                if (response.code() == 200) {
                    val dataResponse = response.body()!!
                    var adapter = MenuAdapter(getArrayFilter(dataResponse))
                    productsView.adapter = adapter
                }
            }

            override fun onFailure(call: Call<MenuResponse>, t: Throwable) {
                    println("error")
            }
        })
    }

    private fun getArrayFilter(dataProducts: MenuResponse?): ArrayList<DataProduct> {
        var  products = ArrayList<DataProduct>()
        for (product in dataProducts!!.data!!.product_lang){
            if (product.lang_id == 1){
                products.add(product)
            }
        }
        return products;
    }




}
