package com.example.test.webservices

class ActivationData {

    var id:Int ?= 0
    var device_id:String ?= ""
    var api_key:String ?= ""
    var hostname:String ?= ""

}