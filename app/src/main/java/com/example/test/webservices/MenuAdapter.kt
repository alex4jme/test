package com.example.test.webservices

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.test.R


class MenuAdapter(private var products: ArrayList<DataProduct>) : RecyclerView.Adapter<MenuAdapter.MenuViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        return MenuViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.menu_item, parent, false))
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val item = products.get(position)
        holder.bindView(item)
    }

    class MenuViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val textView: TextView
        init {
            textView = itemView.findViewById(R.id.name)
        }
        fun bindView(item: DataProduct) {
            textView.text = item.name
        }
    }

}