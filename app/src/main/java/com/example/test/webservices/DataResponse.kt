package com.example.test.webservices

class DataResponse {

    var response:String ? = ""
    var data: Activation ?= null
    var error: String ?= ""
}