package com.example.test.webservices

class DataProduct {
    var product_id:Int ?= 0
    var lang_id:Int ?= 0
    var name:String ?= ""
    var short_description:String ?= ""
    var description:String ?= ""
    var tags:String ?= ""
}