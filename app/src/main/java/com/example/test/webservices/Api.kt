package com.example.test.webservices

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface Api {
    @POST("/api/v2/license/activate")
    fun postLicenseActivate(@Body postModel: DataRequest):Call<DataResponse>

    @GET("/api/v2/products/menu")
    fun getMenu(@Header("api-key") apiKey:String?, @Header("device-id") deviceId:String?):Call<MenuResponse>

}