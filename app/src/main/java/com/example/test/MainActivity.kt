package com.example.test

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.example.test.webservices.Api
import com.example.test.webservices.DataRequest
import com.example.test.webservices.DataResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    private lateinit var serial: EditText
    private lateinit var hostname: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        serial = findViewById(R.id.serial)
        hostname = findViewById(R.id.hostname)
        button = findViewById(R.id.login)

        button.setOnClickListener {
            getCurrentData()
        }
    }


    internal fun getCurrentData() {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://otf2.kubilabs.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(Api::class.java)
        val data = DataRequest()


        val deviceId: String = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        val model: String = Build.MODEL


        data.serial = serial.getText().toString()
        data.hostname = hostname.getText().toString()
        data.device_id = deviceId
        data.device_information = model

        val call = service.postLicenseActivate(data)
        call.enqueue(object : Callback<DataResponse> {
            override fun onResponse(call: Call<DataResponse>, response: Response<DataResponse>) {
                if (response.code() == 201) {
                    val dataResponse = response.body()!!
                    saveLocalStorage(dataResponse!!.data!!.activation!!.device_id, dataResponse!!.data!!.activation!!.api_key)
                    val intent = Intent(this@MainActivity, MenuActivity::class.java)
                    startActivity(intent)
                }
            }

            override fun onFailure(call: Call<DataResponse>, t: Throwable) {
               
            }
        })
    }

    private fun saveLocalStorage(deviceId: String?, apiKey: String?) {
        val preferences = getSharedPreferences("preferences", Context.MODE_PRIVATE)
        var editor = preferences.edit()
        editor.putString("apiKey", apiKey)
        editor.putString("deviceId", deviceId)
        editor.commit()
    }

}
